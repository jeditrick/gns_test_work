<?php

namespace Acme\GnsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AcmeGnsBundle:Default:index.html.twig', array('name' => $name));
    }
}
