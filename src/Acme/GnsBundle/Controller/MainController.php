<?php

    namespace Acme\GnsBundle\Controller;

    use Acme\GnsBundle\Entity\Movie;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\Request;

    class MainController extends Controller
    {
        public function indexAction()
        {
            $movies = $this->getDoctrine()
                ->getRepository('AcmeGnsBundle:Movie')
                ->findBy(
                    [
                        'isActive' => true,
                    ]
                );

            return $this->render('AcmeGnsBundle:Main:index.html.twig', array('movies' => $movies));
        }

        public function addMovieAction(Request $request)
        {
            $movie = new Movie();
            $em = $this->getDoctrine()->getManager();
            $form = $this->createFormBuilder($movie)
                ->add('name', 'text', array(
                    'required' => false,
                ))
                ->add('year', 'text', array(
                    'required' => false,
                ))
                ->add('save', 'submit', array('label' => 'Add Movie'))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $movie->setName($form->get('name')->getData());
                $movie->setYear($form->get('year')->getData());
                $movie->setIsActive(true);
                $em->persist($movie);
                $em->flush();

                return $this->redirect('/');
            }

            return $this->render('AcmeGnsBundle:Main:add_movie.html.twig', array(
                'form' => $form->createView(),
            ));
        }
    }
